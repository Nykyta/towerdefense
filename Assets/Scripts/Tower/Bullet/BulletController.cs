using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletController : MonoBehaviour
{
    public bool isContacted;
    public Constants.BulletType bulletType;
    public List<Unit> onView = new List<Unit>();

    private float _destroyTime = 6f;
    private int _damage;

    [SerializeField]
    private Rigidbody2D _rb;
    [SerializeField]
    private Image _img;
    [SerializeField]
    private Sprite _rocketSprite;
    [SerializeField]
    private float _speed = 100f;

    private void Start() {
        StartCoroutine(Shot());
    }

    private void FixedUpdate()
    {
        if (!isContacted)
            _rb.velocity = transform.up.normalized * _speed;
        else
            _rb.velocity = Vector2.zero;
    }

    public int GetDamage() {
        return _damage;
    }

    public void Init(int damage, Constants.BulletType bulletType) {
        switch (bulletType) {
            case Constants.BulletType.rocket:
                _img.sprite = _rocketSprite;
                break;
        }
        isContacted = false;
        this.bulletType = bulletType;
        this._damage = damage;
    }

    private IEnumerator Shot() {
        yield return new WaitForSeconds(_destroyTime);
        EventManager.OnPlaySound(Constants.AudioValue.Shot);
        EventManager.OnDespawnBullet(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        Unit unit = collision.gameObject.GetComponent<Unit>();
        onView.Add(unit);
    }
}
