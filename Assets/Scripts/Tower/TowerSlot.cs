using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class TowerSlot : MonoBehaviour
{   
    [SerializeField]
    private UnitData Info;
    [SerializeField]
    private Image towerImg;
    [SerializeField]
    private TMP_Text Health;
    [SerializeField]
    private TMP_Text Damage;
    [SerializeField]
    private TMP_Text Cost;
    [SerializeField]
    private TMP_Text TimeShoot;
    [SerializeField]
    private TMP_Text Distance;
    [SerializeField]
    private TMP_Text Bullet;
    // Start is called before the first frame update
    void Awake()
    {
        FillSlot();
    }

    // Update is called once per frame
    private void FillSlot() {
        towerImg.sprite = Info.sprite;
        Health.text = Info.health.ToString();
        Damage.text = Info.damage.ToString();
        Cost.text = Info.cost.ToString();
        TimeShoot.text = Info.timeShoot.ToString();
        Distance.text = Info.distanceOfView.ToString();
        Bullet.text = Info.BulletType.ToString();
        ShakeTower();
    }
     private void ShakeTower() {
        towerImg.transform.DOLocalRotate(new Vector3(0f, 0f, -12f), 2f).SetLoops(-1, LoopType.Yoyo);
    }
}
