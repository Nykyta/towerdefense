using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TowerShoot : MonoBehaviour
    {
    
    private bool _isCanAttack = false;
    private bool _isBulletSpawning = false;
    private bool _isShootingStarted = false;

    [SerializeField]
    private List<DefaultUnit> _enemyInView;
    [SerializeField]
    private TowerController _towerController;
    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private GameObject _bulletSpawnPlace;

    private void FixedUpdate() {
        CheckEnemyInView();
        if (_isCanAttack) {
            if (!_isShootingStarted) {
                StartCoroutine(Shot());
            }
            Rotate(transform, ChooseEnemy().transform.position);
        }
        ReloadCircle();
    }

    private void Rotate(Transform transform, Vector3 point) {
        var direction = (point - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        var ea = transform.eulerAngles;
        ea.z = angle - 90f;
        transform.DORotate(ea, 0.3f);
    }

    private void ReloadCircle() {
        if (_isBulletSpawning) {
            _towerController.reloadCircle.gameObject.SetActive(true);
            _towerController.reloadCircle.fillAmount += 1 / _towerController.Data.timeShoot * Time.deltaTime;
        }
        else {
            _towerController.reloadCircle.gameObject.SetActive(false);
        }
    }

    private DefaultUnit ChooseEnemy() {
        DefaultUnit _mostDangerousEnemy = _enemyInView[0];
        for (int i = 0; i < _enemyInView.Count - 1; i++) {
            if(_mostDangerousEnemy.unityQuality() < _enemyInView[i].unityQuality()) {
                _mostDangerousEnemy = _enemyInView[i];
            }
        }
        return _mostDangerousEnemy;
    }

    private void CheckEnemyInView() {
        _isCanAttack = false;
        for (int i = 0; i < _enemyInView.Count; i++) {
            if(_enemyInView[i] == null) {
                _enemyInView.RemoveAt(i);
            }
            else {
                _isCanAttack = true;
            }
        }
    }

    private IEnumerator Shot() {
        yield return new WaitForSeconds(0.6f);
        while (_isCanAttack && !_isBulletSpawning) {
            _isShootingStarted = true;
            _isBulletSpawning = true;
            _towerController.reloadCircle.fillAmount = 0;
            EventManager.OnSpawnBullet(_towerController, _bulletSpawnPlace.transform.position, transform.rotation, Constants.TAG_FRIENDLY_BULLET);
            EventManager.OnPlaySound(Constants.AudioValue.Shot);

            yield return new WaitForSeconds(_towerController.Data.timeShoot);
            _isBulletSpawning = false;
        }
        _isShootingStarted = false;
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag(Constants.TAG_ENEMY_UNIT) && !_enemyInView.Contains(collision.GetComponent<DefaultUnit>())) {
            _enemyInView.Add(collision.transform.GetComponent<DefaultUnit>());
        }
    }

    public void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.CompareTag(Constants.TAG_ENEMY_UNIT) && _enemyInView.Contains(collision.GetComponent<DefaultUnit>())) {
            _enemyInView.Remove(collision.transform.GetComponent<DefaultUnit>());
        }
    }
}
