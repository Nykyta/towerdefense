using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class TowerController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    public Image reloadCircle;
    public CircleCollider2D colider;

    [SerializeField]
    private UnitData _data;
    [SerializeField]
    private Image _towerImg;
    [SerializeField]
    private Slider _hpSlider;
    [SerializeField]
    private Button _fixBtn;
    [SerializeField]
    private TMP_Text _fixPriceTxt;
    [SerializeField]
    private Image _deletingImg;

    private float _deleteTime = 2;
    private int _fixPrice;
    private int _startHealth;
    private int _health;
    private TowerPlaceController _shopParent;
    private const string RADIUS_SKILLS = "RadiusSkills";


    public void OnPointerDown(PointerEventData eventData) {
        _deletingImg.DOFillAmount(1, _deleteTime).SetEase(Ease.Flash).OnComplete(() => {
            EventManager.OnPlaySound(Constants.AudioValue.FixTower);
            _shopParent.isFree = true;
            GameData.getInstance().Coins += _data.cost / 2;
            EventManager.OnUpdateUIText();
            Destroy(gameObject);
        });
    }

    public void OnPointerUp(PointerEventData eventData) {
        _deletingImg.fillAmount = 0;
        _deletingImg.DOKill();
    }

    public void InitTower(UnitData data) {
        _data = data;
        if (colider) {
            colider.radius = data.distanceOfView;
        }
        else {
            GetComponent<CircleCollider2D>().radius = data.distanceOfView;
        }
        _towerImg.sprite = data.sprite;
        InitHp();
    }

    private void Start() {
        _shopParent = GetComponentInParent<TowerPlaceController>();
        if (_fixBtn) {
            _fixBtn.onClick.AddListener(delegate { FixTower(false); });
            _fixBtn.gameObject.SetActive(false);
            _fixPriceTxt.gameObject.SetActive(false);
        }
    }

    private void Update() {
        if(gameObject.name != RADIUS_SKILLS) {
            CheckFix();
        }
    }

    private void CheckFix() {
        if (_health < _startHealth) {
            _fixPriceTxt.gameObject.SetActive(true);
            _fixBtn.gameObject.SetActive(true);
            _fixPrice = _data.cost / 2;
            _fixPriceTxt.text = Constants.MONEY_SIGN + _fixPrice.ToString();
            _fixBtn.interactable = _shopParent.CheckMoneyForBuy(_fixPrice);
        }

        Color color;
        if (!_shopParent.CheckMoneyForBuy(_fixPrice)) {
            color = Color.red;
        }
        else {
            color = Color.green;
        }
        _fixPriceTxt.DOColor(color, 0.7f);
    }

    private void UpdateHp(int count) {
        _health += count;
        _hpSlider.value = _health;
        BadStateAnimation();
        CheckDeath();
    }

    private void BadStateAnimation() {
        if (_hpSlider.value < 20f) {
            TowerColorIndicator(Color.red);
        }
    }
    private void TowerColorIndicator(Color color) {
        _towerImg.DOColor(color, 0.7f).OnComplete(() => {
            _towerImg.DOColor(Color.white, 0.7f);
        });
    }

    private void InitHp() {
        _health = _data.health;
        _startHealth = _health;
        _hpSlider.maxValue = _health;
        _hpSlider.value = _hpSlider.maxValue;
    }

    private void FixTower(bool isFree) {
        if (gameObject.name != RADIUS_SKILLS) { 
        EventManager.OnPlaySound(Constants.AudioValue.FixTower);
        _health = _startHealth;
        _hpSlider.value = _health;
        TowerColorIndicator(Color.green);
        _fixPriceTxt.gameObject.SetActive(false);
        _fixBtn.gameObject.SetActive(false);
        if (!isFree) {
            GameData.getInstance().Coins -= _fixPrice;
            EventManager.OnUpdateUIText();
        }
        }
    }

    private void CheckDeath() {
        if (_health <= 0) {
            EventManager.OnSpawnExplosion(transform.position);
            _shopParent.isFree = true;
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag(Constants.TAG_ENEMY_BULLET)) {
            var bullet = collision.gameObject.GetComponent<BulletController>();
            EventManager.OnDespawnBullet(collision.gameObject);
            UpdateHp(-bullet.GetDamage());
        }
    }

    private void OnEnable() {
        EventManager.AddHealthEvent += FixTower;
    }

    private void OnDisable() {
        EventManager.AddHealthEvent -= FixTower;
    }

    public UnitData Data {
        set {
            _data = value;
        }
        get {
            return _data;
        }
    }
}

