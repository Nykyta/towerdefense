using System.Collections;
using UnityEngine;
using DG.Tweening;

public class ItemIconAnimation : MonoBehaviour
{
    [SerializeField]
    private TowerPlaceController _towerPlaceContr;
    private const float OFFSET = 115f;
    private const float TIME_TO_OFFSET = 0.2f;
    private Vector3 VECTOR_FOR_PUNCH = new Vector3(0f, 0f, 15f);
    private const float TIME_TO_PUNCH = 0.3f;

    [SerializeField]
    private GameObject[] icons;

    private void OnEnable() {
        StartCoroutine(ShowIcons());
    }

    private void OnDisable() {
        CloseIcons();
    }

    private IEnumerator ShowIcons() {
        var time = new WaitForSeconds(0.1f);
        _towerPlaceContr.isAnimPlaying = true;
        icons[0].transform.DOLocalMoveY(OFFSET, TIME_TO_OFFSET).OnComplete(() => icons[0].transform.DOPunchRotation(VECTOR_FOR_PUNCH, TIME_TO_PUNCH));
        yield return time;
        icons[1].transform.DOLocalMoveX(OFFSET, TIME_TO_OFFSET).OnComplete(() => icons[1].transform.DOPunchRotation(VECTOR_FOR_PUNCH, TIME_TO_PUNCH));
        yield return time;
        icons[2].transform.DOLocalMoveY(-OFFSET, TIME_TO_OFFSET).OnComplete(() => icons[2].transform.DOPunchRotation(VECTOR_FOR_PUNCH, TIME_TO_PUNCH));
        yield return time;
        icons[3].transform.DOLocalMoveX(-OFFSET, TIME_TO_OFFSET).OnComplete(() => icons[3].transform.DOPunchRotation(VECTOR_FOR_PUNCH, TIME_TO_PUNCH)); _towerPlaceContr.isAnimPlaying = false;
    }

    private void CloseIcons() {
        for (int i = 0; i< icons.Length; i++) {
            icons[i].transform.localPosition = Vector3.zero;
        }
    }
}
