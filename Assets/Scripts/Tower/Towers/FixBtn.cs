using UnityEngine;
using DG.Tweening;

public class FixBtn : MonoBehaviour
{
    private float _scaleSpeed = 2f;

    private void OnEnable() {
        StartPulseAnim();
    }

    private void OnDisable() {
        transform.DOKill();
    }

    private void StartPulseAnim() {
        transform.localScale = new Vector3(1, 1, 1);
        transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), _scaleSpeed).SetLoops(-1, LoopType.Yoyo);
    }
}
