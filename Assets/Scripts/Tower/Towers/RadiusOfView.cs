using UnityEngine;
using UnityEngine.UI;

public class RadiusOfView : MonoBehaviour
{
    [SerializeField]
    private TowerShoot _towerShoot;
    [SerializeField]
    private TowerController _towerController;
    [SerializeField]
    private Image _viewZone;

    private void Start() {
        _viewZone.GetComponent<RectTransform>().sizeDelta = new Vector3(_towerController.colider.radius, _towerController.colider.radius, 0);    
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        _towerShoot.OnTriggerEnter2D(collision);
    }

    private void OnTriggerExit2D(Collider2D collision) {
        _towerShoot.OnTriggerExit2D(collision);
    }
}
