using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopCell : MonoBehaviour
{
    [SerializeField]
    private Image _towerImg;
    [SerializeField]
    private TMP_Text _priceText;

    public void Init(Sprite sprite, int cost) {
        _towerImg.sprite = sprite;
        _priceText.text = Constants.MONEY_SIGN + cost.ToString();
    }

    public TMP_Text PriceText {
        get {
            return _priceText;
        }
        set {
            _priceText = value;
        }
    }
}
