using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TowerPlaceController : MonoBehaviour {
    public static bool isGameGoing;
    public bool isFree = true;
    public bool isDamaged = false;
    public bool isAnimPlaying;
    [SerializeField]
    private Button _shopBtn;
    [SerializeField]
    private List<UnitData> _towers;
    [SerializeField]
    private List<Button> _shopCellsBtns;
    [SerializeField]
    private GameObject _cellsPanel;
    [SerializeField]
    private GameObject _towerPrefab;
    private List<ShopCell> _shopCells = new List<ShopCell>();

    private void Start() {
        _shopBtn.onClick.AddListener(PlaceClick);
        for (int i = 0; i < _shopCellsBtns.Count; i++) {
            _shopCells.Add(_shopCellsBtns[i].GetComponent<ShopCell>());
            _shopCells[i].Init(_towers[i].sprite, _towers[i].cost);
        }
    }

    private void Update() {
        _shopBtn.interactable = isFree & isGameGoing;
        CheckBuyPossib(Color.red);
    }

    public bool CheckMoneyForBuy(int neededMoney) {
        if (neededMoney <= GameData.getInstance().Coins)
            return true;
        return false;
    }

    public void BuyTower(Button btn) {
        if (GameData.getInstance().Coins < _towers[_shopCellsBtns.IndexOf(btn)].cost || !isGameGoing || isAnimPlaying)
            return;
        isFree = false;
        EventManager.OnPlaySound(Constants.AudioValue.ShopClick);
        _cellsPanel.SetActive(false);
        var tower = Instantiate(_towerPrefab, gameObject.transform).GetComponentInChildren<TowerController>();
        tower.InitTower(_towers[_shopCellsBtns.IndexOf(btn)]);
        tower.transform.localPosition = Vector3.zero;
        tower.transform.localScale = new Vector3(1, 1, 1);
        GameData.getInstance().Coins -= _towers[_shopCellsBtns.IndexOf(btn)].cost;
        EventManager.OnUpdateUIText();
    }

    private void CheckBuyPossib(Color color) {
        for (int i = 0; i < _shopCellsBtns.Count; i++) {
            if (_towers[i].cost > GameData.getInstance().Coins) {
                _shopCells[i].PriceText.DOColor(color, 0.7f);
            }
            else {
                _shopCells[i].PriceText.DOColor(Color.green, 0.7f);
            }
            _shopCellsBtns[i].interactable = _towers[i].cost < GameData.getInstance().Coins;
        }
    }

    public void PlaceClick() {
        _cellsPanel.SetActive(!_cellsPanel.active);
        EventManager.OnPlaySound(Constants.AudioValue.ShopClick);
    }
}
