using UnityEngine;

public class EventManager {
    #region SpawnAndDespawn
    public delegate void SpawnUnitEvent(UnitObjectData obj);
    public static event SpawnUnitEvent SpawnNewUnit;

    public delegate void SpawnBonusEvent(BonusObjectData obj);
    public static event SpawnBonusEvent SpawnNewBonus;

    public delegate void SpawnBulletEvent(BulletObjectData obj);
    public static event SpawnBulletEvent SpawnNewBullet;

    public delegate void SpawnExplosionEvent(ExplosionObjectData obj);
    public static event SpawnExplosionEvent SpawnNewExplosion;

    public delegate void DamageText(DamageTextObjectData obj);
    public static event DamageText DamageTextEvent;

    public delegate void DespawnEvent(GameObject obj);
    public static event DespawnEvent DespawnUnit;
    public static event DespawnEvent DespawnDamageText;
    public static event DespawnEvent DespawnBullet;
    public static event DespawnEvent DespawnExplosion;
    public static event DespawnEvent DespawnBonus;
    public static void OnSpawnUnit(Constants.Enemies id, Vector3 pos, GameObject waypoints, string tag) => SpawnNewUnit?.Invoke(new UnitObjectData(id, pos, waypoints, tag));
    public static void OnSpawnBullet(TowerController towerController, Vector3 pos, Quaternion rot, string tag) => SpawnNewBullet?.Invoke(new BulletObjectData(towerController, pos, rot, tag));
    public static void OnSpawnExplosion(Vector3 pos) => SpawnNewExplosion?.Invoke(new ExplosionObjectData(pos));
    public static void OnSpawnBonus(Vector3 pos) => SpawnNewBonus?.Invoke(new BonusObjectData(pos));
    public static void OnDespawnUnit(GameObject obj) => DespawnUnit?.Invoke(obj);
    public static void OnDespawnDamageText(GameObject obj) => DespawnDamageText?.Invoke(obj);
    public static void OnDespawnBullet(GameObject obj) => DespawnBullet?.Invoke(obj);
    public static void OnDespawnExplosion(GameObject obj) => DespawnExplosion?.Invoke(obj);
    public static void OnDespawnBonus(GameObject obj) => DespawnBonus?.Invoke(obj);
    public static void OnDamageText(int value, Vector3 pos) => DamageTextEvent?.Invoke(new DamageTextObjectData(value, pos));

    #endregion

    #region BonusEvents
    public delegate void BonusActive(Constants.Bonuses bonusType);
    public static event BonusActive StartBonusEvent;

    public delegate void ChangeSpeed();
    public static event ChangeSpeed ChangeSpeedEvent;

    public delegate void AddHealth(bool isFree);
    public static event AddHealth AddHealthEvent;

    public delegate void AviaBombard();
    public static event AviaBombard AviaBombardEvent;

    public static void OnStartBonus(Constants.Bonuses bonusType) => StartBonusEvent?.Invoke(bonusType);
    public static void OnBombard() => AviaBombardEvent?.Invoke();
    public static void OnChangeSpeed() => ChangeSpeedEvent?.Invoke();
    public static void OnAddHealth(bool isFree) => AddHealthEvent?.Invoke(isFree);
    #endregion

    #region AudioEvents
    public delegate void PlaySound(Constants.AudioValue soundName);
    public static event PlaySound PlaySoundEvent;

    public delegate void SoundOnOff();
    public static event SoundOnOff SoundOnOffEvent;
    public delegate void MusicOnOff();
    public static event MusicOnOff MusicOnOffEvent;

    public static void OnPlaySound(Constants.AudioValue soundName) => PlaySoundEvent?.Invoke(soundName);
    public static void OnSoundOnOff() => SoundOnOffEvent?.Invoke();
    public static void OnMusicOnOff() => MusicOnOffEvent?.Invoke();
    #endregion

    #region Menu

    public delegate void UpdateUIText();
    public static event UpdateUIText UpdateUITextEvent;

    public delegate void BossSliderBar();
    public static event BossSliderBar StateCountOfUnitsEvent;
    public static event BossSliderBar ChangeSliderBarEvent;

    public delegate void �heckPanel();
    public static event �heckPanel DefeatedPanelEvent;
    public static event �heckPanel WinPanelEvent;

    public static void OnUpdateUIText() => UpdateUITextEvent?.Invoke();
    public static void OnWinPanel() => WinPanelEvent?.Invoke();
    public static void OnDefeatedPanel() => DefeatedPanelEvent?.Invoke();
    public static void OnStateCountOfUnits() => StateCountOfUnitsEvent?.Invoke();
    public static void OnChangeSliderBar() => ChangeSliderBarEvent?.Invoke();
    #endregion

    #region Other
    public delegate void DamageEvent(int dmg);
    public static event DamageEvent SetDamage;
    public delegate void StarsEvent(int castleHealth);
    public static event StarsEvent SetMark;

    public static void OnSetDamage(int dmg) => SetDamage?.Invoke(dmg);
    public static void OnSetStars(int castleHealth) => SetMark?.Invoke(castleHealth);
    #endregion

}