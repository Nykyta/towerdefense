using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class BonusManager : MonoBehaviour
{
    [SerializeField]
    public Transform AviaBlock;
    [SerializeField]
    public GameObject Explosions;
    private const float TIME_TO_FLY = 30f;
    private const float X_FOR_PLANE = 1300f;
    [SerializeField]
    private Button _aviaBtn;
    [SerializeField]
    private Button _healthBtn;
    [SerializeField]
    private Button _speedBtn;
    [SerializeField]
    private TMP_Text _aviaCount;
    [SerializeField]
    private TMP_Text _healthCount;
    [SerializeField]
    private TMP_Text _speedCount;

    private void Start() {
        SubscribeListeners();
        _aviaBtn.enabled = false;
        _healthBtn.enabled = false;
        _speedBtn.enabled = false;
    }
    private void OnEnable() {
        EventManager.StartBonusEvent += BonusTypeObserver;
    }

    private void OnDisable() {
        EventManager.StartBonusEvent -= BonusTypeObserver;
    }

    private void BonusTypeObserver(Constants.Bonuses type) {
        switch (type) {
            case Constants.Bonuses.avia:
                _aviaCount.text = (int.Parse(_aviaCount.text) + 1).ToString();
                _aviaBtn.enabled = true;
                break;
            case Constants.Bonuses.addhealth:
                _healthCount.text = (int.Parse(_healthCount.text) + 1).ToString();
                _healthBtn.enabled = true;
                break;
            case Constants.Bonuses.lowspeed:
                _speedCount.text = (int.Parse(_speedCount.text) + 1).ToString();
                _speedBtn.enabled = true;
                break;
        }
    }

    private void Avia() {
        Time.timeScale = 1;
        int col = int.Parse(_aviaCount.text) - 1;
        _aviaCount.text = col.ToString();
        EventManager.OnPlaySound(Constants.AudioValue.PlaneSound);
        AviaBlock.gameObject.SetActive(true);
        AviaBlock.DOLocalMoveX(X_FOR_PLANE, TIME_TO_FLY).OnComplete(() => { ReturnToBase();  if (col == 0) { _aviaBtn.enabled = false; } });
        StartCoroutine(AviaEffects());
    }

    private IEnumerator AviaEffects() {
        var time_before_bombard = new WaitForSeconds(5f);
        var time_before_explosions = new WaitForSeconds(4f);
        var time_for_bombard = new WaitForSeconds(3f);
        yield return time_before_bombard;
        EventManager.OnPlaySound(Constants.AudioValue.Bombard);
        yield return time_before_explosions;
        Explosions.SetActive(true);
        EventManager.OnBombard();
        yield return time_for_bombard;
        Explosions.SetActive(false);
    }

    private void ReturnToBase() {
        AviaBlock.DOLocalMoveX(-X_FOR_PLANE, 0f).OnComplete(() => { AviaBlock.gameObject.SetActive(false); _aviaBtn.enabled = true; });
    }

    private void AddHealth() {
        int col = int.Parse(_healthCount.text) -1;
        _healthCount.text = col.ToString();
        EventManager.OnAddHealth(true);
        if (col == 0) {
            _healthBtn.enabled = false;
        }
    }

    private void LowSpeed() {
        int col = int.Parse(_speedCount.text) - 1;
        _speedCount.text = col.ToString();
        EventManager.OnChangeSpeed();
        if (col == 0) {
            _speedBtn.enabled = false;
        }
    }

    private void SubscribeListeners() {
        _aviaBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); _aviaBtn.enabled = false; Avia(); });
        _healthBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); AddHealth(); });
        _speedBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); LowSpeed(); });
    }
}
