using System.Collections.Generic;
using UnityEngine;

public class AudioManager :MonoBehaviour 
{
    [SerializeField]
    private List<AudioClip> Clips = new List<AudioClip>();
    [SerializeField]
    private AudioSource AudioObjSounds;
    [SerializeField]
    private AudioSource AudioObjMusic;
    private static AudioManager _instance;
    private GameData gameData = GameData.getInstance();

    void Awake() {
        
        if (_instance != null) {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(gameObject);
        

    }

    private void Start() {
        gameData.LoadData();
        AudioObjMusic.mute = gameData.Music;
        AudioObjSounds.mute = gameData.Sounds;
    }

    public static AudioManager Instance {
        get {
            if (_instance == null) {
                _instance = new AudioManager();
            }
            return _instance;
        }
    }


    private void OnEnable() {
        EventManager.PlaySoundEvent += PlayAudio;
        EventManager.SoundOnOffEvent += ChangeSoundStatus;
        EventManager.MusicOnOffEvent += ChangeMusicStatus;
    }
    private void OnDisable() {
        EventManager.PlaySoundEvent -= PlayAudio;
        EventManager.SoundOnOffEvent -= ChangeSoundStatus;
        EventManager.MusicOnOffEvent -= ChangeMusicStatus;
    }

    private void ChangeSoundStatus() {
        AudioObjSounds.mute = !AudioObjSounds.mute;
    }

    private void ChangeMusicStatus() {
        AudioObjMusic.mute = !AudioObjMusic.mute;
    }

    private void PlayAudio(Constants.AudioValue audioName) {
        AudioObjSounds.PlayOneShot(Clips[(int)audioName]);
    }
}
