using UnityEngine;

public class GameData{
	
	public int Coins;
	public int Level;
	public int Days;
	public int CountUnitsOnLevel;
	public bool Music;
	public bool Sounds;
	private static GameData instance;

	public static GameData getInstance() {
		if (instance == null) {
			instance = new GameData();
		}
		return instance;
	}
	public void SaveData() {
		PlayerPrefs.SetInt(Constants.COINS_KEY, Coins);
		PlayerPrefs.SetInt(Constants.DAYS_KEY, Days);
		PlayerPrefs.SetInt(Constants.LEVELS_KEY, Level);
		PlayerPrefs.SetInt(Constants.COUNT_UNITS_ON_LEVEL, CountUnitsOnLevel);
		PlayerPrefs.SetInt (Constants.MUSIC, Music? 1:0 );
		PlayerPrefs.SetInt(Constants.SOUNDS, Sounds ? 1 : 0);
	}
	public void LoadData() {
		Coins = PlayerPrefs.GetInt(Constants.COINS_KEY);
		Days = PlayerPrefs.GetInt(Constants.DAYS_KEY);
		Level = PlayerPrefs.GetInt(Constants.LEVELS_KEY);
		CountUnitsOnLevel = PlayerPrefs.GetInt(Constants.COUNT_UNITS_ON_LEVEL);
		Music = (PlayerPrefs.GetInt(Constants.MUSIC) != 0);
		Sounds = (PlayerPrefs.GetInt(Constants.SOUNDS) != 0);
	}

}

