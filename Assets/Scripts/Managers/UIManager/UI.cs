using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour
{
    #region PANELS
    [SerializeField]
    private GameObject _unitsPanel;
    [SerializeField]
    private GameObject _parametersPanel;
    [SerializeField]
    private GameObject _pauseMenuPanel;
    [SerializeField]
    private GameObject _winPanel;
    [SerializeField]
    private GameObject _defeatedPanel;
    [SerializeField]
    private GameObject _startPanel;
    [SerializeField]
    private GameObject _starsPanel;
    #endregion
    #region BUTTONS
    [SerializeField]
    private Button _unitsPanelButton;
    [SerializeField]
    private Button _parmetersPanelButton;
    [SerializeField]
    private Button _cancelParametersPanelBtn;
    [SerializeField]
    private Button _cancelUnitsPanelBtn;
    [SerializeField]
    private Button _pausePanelButton;
    [SerializeField]
    private Button _continueButton;
    [SerializeField]
    private Button _restartButton;
    [SerializeField]
    private Button _mainMenuButton;
    [SerializeField]
    private Button _mainMenuButtonWinPanel;
    [SerializeField]
    private Button _mainMenuButtonDefeatedPanel;
    [SerializeField]
    private Button _restartButtonWinPanel;
    [SerializeField]
    private Button _nextLevelButton;
    [SerializeField]
    private Button _SpeedX1Btn;
    [SerializeField]
    private Button _SpeedX2Btn;
    #endregion
    #region PARAMETERS
    [SerializeField]
    private TextMeshProUGUI _moneyText;
    [SerializeField]
    private TextMeshProUGUI _levelText;
    [SerializeField]
    private TextMeshProUGUI _castlHealthText;
    [SerializeField]
    private TextMeshProUGUI _unitsOnMapText;
    [SerializeField]
    private TextMeshProUGUI _startTimerText;

    #endregion
    #region Objects
    private CastleController _castle;
    #endregion
    private float _startTimer;
    private float _startCooldown = 3f;
    private GameData gameData = GameData.getInstance();
    private void Start()
    {
        InitUI();
        StartCoroutine(StartTimer());
    }


    private void InitTimer() 
    {
        _startTimer = _startCooldown;
        Time.timeScale = 0;
        _startTimerText.text = ((int)_startTimer).ToString();
        CheckSpeedBtnState(); 
    }

    IEnumerator StartTimer() 
    {
        TowerPlaceController.isGameGoing = false;
        if (_startTimer > 0)
        {
            _startTimerText.text = ((int)_startTimer).ToString();
            _startTimer -= 1;
            yield return new WaitForSecondsRealtime(1f);
            StartCoroutine(StartTimer());
        }
        else
        {
            _startPanel.SetActive(false);
            Time.timeScale = 1;
            CheckSpeedBtnState();
            if(_pausePanelButton.interactable)
                TowerPlaceController.isGameGoing = true;
        }
    }

    private void SetGameSpeed( int speed) 
    {
        Time.timeScale = speed;
    }

    private void CheckSpeedBtnState() 
    {
        if (Time.timeScale == 0)
        {
            _SpeedX1Btn.enabled = false;
            _SpeedX2Btn.enabled = false;
        }
        else
        {
            _SpeedX1Btn.enabled = true;
            _SpeedX2Btn.enabled = true;
        }
    }

    private void SetActivePanel(GameObject obj)
    {
        if (obj.activeInHierarchy) obj.SetActive(!obj.activeInHierarchy);
        else obj.SetActive(!obj.activeInHierarchy);
        _parmetersPanelButton.gameObject.SetActive(!_parmetersPanelButton.gameObject.activeInHierarchy);
    }

    private void SetScene(string scene)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(scene);
    }

    private void RestartLevel()
    {
        EventManager.OnPlaySound(Constants.AudioValue.ButtonClick);
        Time.timeScale = 1;
        if (SceneManager.GetActiveScene().name == Constants.SANDBOX_SCENE && GameData.getInstance().Coins > Constants.SANDBOX_COST)
        {
            GameData.getInstance().Coins -= 50;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (SceneManager.GetActiveScene().name != Constants.SANDBOX_SCENE)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void ContinueGame()
    {
        EventManager.OnPlaySound(Constants.AudioValue.ButtonClick);
        Time.timeScale = 1;
        _pauseMenuPanel.SetActive(!_pauseMenuPanel.activeInHierarchy);
        CheckSpeedBtnState();
        _pausePanelButton.interactable = true;
        TowerPlaceController.isGameGoing = true;
    }

    private void NextLevel() 
    {
        string name = SceneManager.GetActiveScene().name;
        int countChar = name.Length - 1;
        string levelStr = "";
        int level = 0;
        for (int i = 0; i < name.Length; i++) // for 10 Levels can additional this function
        {
            if (i == name.Length-1)
                levelStr += name[i]; 
        }
        level = int.Parse(levelStr);
        level += 1;
        Time.timeScale = 1;
        SceneManager.LoadScene(Constants.LEVEL_BASE_NAME + level.ToString());
    }

    private void SetPauseMenu()
    {
        EventManager.OnPlaySound(Constants.AudioValue.ButtonClick);
        if (_pauseMenuPanel.activeInHierarchy)
        {
            Time.timeScale = 1;
            _pauseMenuPanel.SetActive(!_pauseMenuPanel.activeInHierarchy);
            TowerPlaceController.isGameGoing = true;
        }
        else
        {
            Time.timeScale = 0;
            _pauseMenuPanel.SetActive(!_pauseMenuPanel.activeInHierarchy);
            TowerPlaceController.isGameGoing = false;
            _pausePanelButton.interactable = false;
        }
        CheckSpeedBtnState();
    }

    private void UpdateUIText()
    {
        int money = int.Parse(_moneyText.text);
        ChangeMoney(money);
        if (SceneManager.GetActiveScene().name == Constants.SANDBOX_SCENE) {
            var day = GameData.getInstance().Days / 4;
            _levelText.text = day.ToString();
        }
        else 
        { 
            _levelText.text = GameData.getInstance().Level.ToString();
        }
        _castlHealthText.text = _castle.Health.ToString();
        _unitsOnMapText.text = GameData.getInstance().CountUnitsOnLevel.ToString();
    }

    private void ChangeMoney(int money)
    {
        int helpMoney = money;
        StartCoroutine(ChangeMoney(helpMoney, money, true));
        StartCoroutine(ChangeMoney(helpMoney, money, false));
    }

    private IEnumerator ChangeMoney(int helpMoney, int money, bool t) 
    {
        for (int i = money; t ? i < GameData.getInstance().Coins : i > GameData.getInstance().Coins; i += t ? 1 : -1) { 
            helpMoney += t ? 1 : -1;
            _moneyText.text = helpMoney.ToString();
            yield return new WaitForSeconds(0.001f);
        }
        yield break;
    }

    private void DefeatedPanelEvent() 
    {
        GameData.getInstance().SaveData();
        Time.timeScale = 0;
        _pausePanelButton.enabled = false;
        _defeatedPanel.SetActive(true);
    }
    private void WinPanelEvent()
    {
        GameData.getInstance().SaveData();
        Time.timeScale = 0;
        _pausePanelButton.enabled = false;
        GameData.getInstance().Level += 1;
        _winPanel.SetActive(true);
        _starsPanel.SetActive(true);
        if (GameData.getInstance().Level > 3) {
            _nextLevelButton.enabled = false;
        }

    }
    private void SubscribeListeners() {
        _unitsPanelButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetActivePanel(_unitsPanel); });
        _parmetersPanelButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetActivePanel(_parametersPanel); });
        _pausePanelButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetPauseMenu(); });


        _cancelParametersPanelBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetActivePanel(_parametersPanel); });
        _cancelUnitsPanelBtn.onClick.AddListener(delegate { SetActivePanel(_unitsPanel); });

        _continueButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); ContinueGame(); });
        _restartButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); RestartLevel(); });
        _restartButtonWinPanel.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); RestartLevel(); });
        _mainMenuButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetScene(Constants.MAIN_MENU); });
        _mainMenuButtonWinPanel.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetScene(Constants.MAIN_MENU); });
        _mainMenuButtonDefeatedPanel.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetScene(Constants.MAIN_MENU); });
        _nextLevelButton.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); NextLevel(); });

        _SpeedX1Btn.onClick.AddListener(() => SetGameSpeed(1));
        _SpeedX2Btn.onClick.AddListener(() => SetGameSpeed(3));
    }

    private void InitUI() 
    {
        SubscribeListeners();
        SubscribeEvents();
        GameObject castle = GameObject.Find(Constants.TAG_CASTLE);
        _castle = castle.GetComponent<CastleController>();
        _moneyText.text = GameData.getInstance().Coins.ToString();
        UpdateUIText();
        InitTimer();
    }

    private void OnDestroy()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        EventManager.UpdateUITextEvent += UpdateUIText;
        EventManager.DefeatedPanelEvent += DefeatedPanelEvent;
        EventManager.WinPanelEvent += WinPanelEvent;
    }
    private void UnsubscribeEvents()
    {
        EventManager.UpdateUITextEvent -= UpdateUIText;
        EventManager.DefeatedPanelEvent -= DefeatedPanelEvent;
        EventManager.WinPanelEvent -= WinPanelEvent;
    }

    private void OnApplicationQuit()
    {
        GameData.getInstance().SaveData();
    }
}
