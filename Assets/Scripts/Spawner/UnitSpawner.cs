using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnitSpawner : MonoBehaviour
{
    [SerializeField] 
    private GameObject _wayPoints;
    [SerializeField] 
    private bool _isSpawnLevel = true;
    [SerializeField]
    private bool _isSpawnRandom = false;
    [SerializeField] 
    private float _spawnCooldownUnit = 3f;
    [SerializeField] 
    private float _spawnCooldownStage = 10f;
    [SerializeField] 
    private float _lengthStageSandbox = 10f;
    [SerializeField] 
    private float _lenghtUnitSandbox = 5f;
    [SerializeField]
    private StageForSpawnData[] _stages;
    [SerializeField]
    private TextMeshProUGUI _stageTimerText;

    private IEnumerator coroutine;


    private void Awake()
    {
        GameData.getInstance().CountUnitsOnLevel = 0;
        GameData.getInstance().Days = 0;
    }
    private void Start()
    {
        Init();
        EventManager.OnUpdateUIText();
    }

    private void CheckCountOnUnit() 
    {
        int countUnits = 0;
        for (int i = 0; i < _stages.Length; i++)
        {
            for (int j = 0; j < _stages[i].units.Length; j++)
            {
                countUnits += 1;
            }
        }
        GameData.getInstance().CountUnitsOnLevel += countUnits;
        EventManager.OnStateCountOfUnits();
    }

    private void Init()
    {
        if (_isSpawnLevel)
        {
            coroutine = SpawnUnitForStage(_spawnCooldownUnit, _spawnCooldownStage);
            StartCoroutine(coroutine);
        }
        if (_isSpawnRandom)
        {
            coroutine = SpawnUnitRandom(_spawnCooldownUnit, _spawnCooldownStage, _lengthStageSandbox, _lenghtUnitSandbox);
            StartCoroutine(coroutine);
        }
        CheckCountOnUnit();
        if (SceneManager.GetActiveScene().name.Equals(Constants.SANDBOX_SCENE)) GameData.getInstance().CountUnitsOnLevel = Constants.SANDBOX_UNIT_COUNT;
    }

    IEnumerator SpawnUnitForStage(float cooldownSpawnUnit, float cooldownSpawnStage) 
    {
        
        for (int i = 0; i < _stages.Length; i++)
        {
            
           for (int j = 0; j < _stages[i].units.Length; j++)
            {
                EventManager.OnSpawnUnit(_stages[i].units[j], transform.position, _wayPoints, tag);
                EventManager.OnChangeSliderBar();
                yield return new WaitForSeconds(cooldownSpawnUnit);
            }
            yield return new WaitForSeconds(cooldownSpawnStage);
        }
        StopCoroutine(coroutine);
    }
    IEnumerator SpawnUnitRandom(float cooldownSpawnUnit, float cooldownSpawnStage, float lengthStage, float lengthUnit)
    {
        for (int i = 0; i < lengthStage; i++)
        {
            for (int j = 0; j < lengthUnit; j++)
            {
                int rnd = Random.Range(0, Constants.COUNT_ENEMIES_TYPES + 1);
                var enemies = Constants.Enemies.Kamikaze;
                switch (rnd)
                {
                    case 1:
                        enemies = Constants.Enemies.Kamikaze;
                        break;
                    case 2:
                        enemies = Constants.Enemies.Warrior;
                        break;
                    case 3:
                        enemies = Constants.Enemies.Shooter;
                        break;
                    case 4:
                        enemies = Constants.Enemies.Tank;
                        break;
                }
                EventManager.OnSpawnUnit(enemies, transform.position, _wayPoints, tag);
                yield return new WaitForSeconds(cooldownSpawnUnit);
            }
            yield return new WaitForSeconds(cooldownSpawnStage);
        }
        _lengthStageSandbox += 1;
        _lenghtUnitSandbox += 0.5f;
        _spawnCooldownUnit -= 0.2f;
        _spawnCooldownStage -= 0.2f;
        GameData.getInstance().Days += 1;
        coroutine = SpawnUnitRandom(_spawnCooldownUnit, _spawnCooldownStage, _lengthStageSandbox, _lenghtUnitSandbox);
        StartCoroutine(coroutine);
    }
   
}


