using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class Menu :MonoBehaviour {
    #region PANELS
    [SerializeField]
    private GameObject _levelPanel;
    [SerializeField]
    private GameObject _fortunePanel;
    [SerializeField]
    private GameObject _preExitPanel;
    [SerializeField]
    private GameObject _TowersPanel;
    #endregion
    #region MENU_PANEL_ITEMS
    [SerializeField]
    private Button _selectLevelBtn;
    [SerializeField]
    private Button _sandBoxModeBtn;
    [SerializeField]
    private Button _settingsBtn;
    [SerializeField]
    private Button _shoopBtn;
    [SerializeField]
    private Button _exitBtn;
    [SerializeField]
    private Button _trelloBtn;
    [SerializeField]
    private Button _sandBtn;
    [SerializeField]
    private Button _closeSelectLevelBtn;
    #endregion
    #region LEVEL_PANEL_ITEMS
    #endregion
    #region SETTINGS_PANEL_ITEMS
    private Vector3 HideSizePanel = new Vector3(0.5f, 0.5f, 0.5f);
    private Vector3 ActiveSizePanel = new Vector3(1f, 1f, 1f);
    #endregion
    #region SHOP_PANEL_ITEMS
    #endregion
    #region PARAMETERS_TEXT
    [SerializeField]
    private TextMeshProUGUI _moneyText;
    [SerializeField]
    private TextMeshProUGUI _levelText;
    #endregion
    #region PRE_EXIT_PANEL
    [SerializeField]
    private Button _noBtn;
    [SerializeField]
    private Button _yesBtn;
    #endregion
    #region Fortune_Wheel_Items
    [SerializeField]
    private Button _wheelbtn;
    [SerializeField]
    private Button _backBtn;
    [SerializeField]
    private TMP_Text _canWeSpinsText;
    #endregion
    #region TOWERS_PANEL
    [SerializeField]
    private Button _infoBtn;
    [SerializeField]
    private Button _closeTowerPanelBtn;
    #endregion

    private void Start() {
        SubscribeListeners();
        UpdateMenuText();
        UpdateWheelText();
    }

    private void SetLinkOnButton(string link) {
        Application.OpenURL(link);
    }
    private void SetScene(string scene) {
        if (scene == Constants.SANDBOX_SCENE && GameData.getInstance().Coins >= 50) {
            GameData.getInstance().Coins -= 50;
        }
        else return;
        SceneManager.LoadScene(scene);
    }

    private void UpdateMenuText() {
        _moneyText.text = GameData.getInstance().Coins.ToString();
        _levelText.text = GameData.getInstance().Level.ToString();
    }

    private void UpdateWheelText() {
        _canWeSpinsText.text = Constants.WHEEL_CAN_SPIN;
    }

    private void IsActivePanel(GameObject panel) {
        if (panel.activeSelf == true) {
            panel.SetActive(false);
            panel.transform.localScale = HideSizePanel;
        }
        else {
            panel.SetActive(true);
            panel.transform.DOScale(ActiveSizePanel, 0.6f);
        }
    }

    private void QuitApplication() {
        GameData.getInstance().SaveData();
        Application.Quit();
    }

    private IEnumerator DisableButton(Button button) {
        button.enabled = false;
        var time = new WaitForSeconds(1f);
        yield return time;
        button.enabled = true;
    }

    private void FortunePanel() {
        _fortunePanel.SetActive(!_fortunePanel.active);
        _infoBtn.enabled = !_infoBtn.enabled;
    }

    private void TowerPanel() {
        _TowersPanel.SetActive(!_TowersPanel.active);
    }

    private void SubscribeListeners() {
        _trelloBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetLinkOnButton(Constants.TRELLO_LINK); });
        _sandBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); SetLinkOnButton(Constants.SANDGAME_LINK); });
        _selectLevelBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); StartCoroutine(DisableButton(_selectLevelBtn)); IsActivePanel(_levelPanel); });
        _closeSelectLevelBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); StartCoroutine(DisableButton(_selectLevelBtn)); IsActivePanel(_levelPanel); });
        _sandBoxModeBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); StartCoroutine(DisableButton(_sandBoxModeBtn)); SetScene(Constants.SANDBOX_SCENE); });
        _exitBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); _preExitPanel.SetActive(true); });
        _noBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); _preExitPanel.SetActive(false); ; });
        _yesBtn.onClick.AddListener(QuitApplication);
        _wheelbtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); FortunePanel(); });
        _backBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); FortunePanel(); });
        _infoBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); TowerPanel(); });
        _closeTowerPanelBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); TowerPanel(); });
    }   
}
