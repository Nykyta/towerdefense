using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AudioButtons :MonoBehaviour {
    [SerializeField]
    private Button MusicBtn;
    [SerializeField]
    private Button SoundBtn;
    [SerializeField]
    private Sprite SoundOn;
    [SerializeField]
    private Sprite SoundOff;
    [SerializeField]
    private Sprite MusicOn;
    [SerializeField]
    private Sprite MusicOff;
    private Image MusicBtnImage;
    private Image SoundBtnImage;
    private GameData gameData = GameData.getInstance();

    private void Start() {
        SubscribeListeners();
        MusicBtnImage = MusicBtn.GetComponent<Image>();
        SoundBtnImage = SoundBtn.GetComponent<Image>();
        InstantiateMusicAndSounds();
    }

    private void InstantiateMusicAndSounds() {
        if (gameData.Music) {
            MusicBtnImage.sprite = MusicOff;
        }
        else { MusicBtnImage.sprite = MusicOn; }

        if (gameData.Sounds) {
            SoundBtnImage.sprite = SoundOff;
        }
        else { SoundBtnImage.sprite = SoundOn; }

    }

    private void ChangeMusicButton() {
        gameData.Music = !gameData.Music;
        InstantiateMusicAndSounds();
    }
    private void ChangeSoundButton() {
        gameData.Sounds = !gameData.Sounds;
        InstantiateMusicAndSounds();
    }

    private void SubscribeListeners() {
        MusicBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); EventManager.OnMusicOnOff(); ChangeMusicButton(); });
        SoundBtn.onClick.AddListener(delegate { EventManager.OnPlaySound(Constants.AudioValue.ButtonClick); EventManager.OnSoundOnOff(); ChangeSoundButton(); });
    }
    
}
