using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Preload : MonoBehaviour
{
    private GameData gameData = GameData.getInstance();

    [SerializeField]
    private Slider slider;
    [SerializeField]
    private TMP_Text progressText;

    private void Start() {
        GameData.getInstance().LoadData();
        Preloading();
        if (GameData.getInstance().Level == 0) GameData.getInstance().Level = 1;
        if (GameData.getInstance().Coins == 0) GameData.getInstance().Coins = 2000;
    }
    private void Preloading() {
        StartCoroutine(LoadAsync());
    }
    private IEnumerator LoadAsync() {
        gameData.LoadData();
        AsyncOperation load = SceneManager.LoadSceneAsync(Constants.MAIN_MENU);
        while (!load.isDone) {
            float progress = Mathf.Clamp01(load.progress / .9f);
            slider.value = progress;
            progressText.text = progress * 100 + "%";
            yield return null;
        }
    }
}
