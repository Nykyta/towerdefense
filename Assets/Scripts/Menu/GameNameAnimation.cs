using UnityEngine;
using DG.Tweening;

public class GameNameAnimation : MonoBehaviour
{
    void Start()
    {
        transform.DOScale(new Vector3(0.7f, 0.7f), 0.7f).SetLoops(-1, LoopType.Yoyo).SetSpeedBased();
    }
}
