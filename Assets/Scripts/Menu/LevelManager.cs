using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    private Dictionary<Button, int> _levels = new Dictionary<Button, int>();
    [SerializeField]
    private int _scenesCount;
    [SerializeField]
    private Button _buttonPrefab;
    [SerializeField]
    private GameObject _content;

    private void Awake() {
        InitLevels();
        SetBtnListener();
    }

    private void OnEnable() {
        CheckAvailableLevels();
    }

    private void InitLevels() {
        for (int i = 0; i < _scenesCount; i++) {
            var button = Instantiate(_buttonPrefab, _content.transform);
            button.GetComponentInChildren<TMP_Text>().text = Constants.LEVEL_NAME_FOR_PREFAB + " " + (i + 1).ToString();
            _levels.Add(button, i + 1);
        }
    }

    private void SetBtnListener() {
        foreach (var item in _levels.Keys) {
            item.onClick.AddListener(delegate { LoadLevel(item); });
        }
    }

    private void LoadLevel(Button btn) {
        EventManager.OnPlaySound(Constants.AudioValue.ButtonClick);
        SceneManager.LoadScene(Constants.LEVEL_BASE_NAME + _levels[btn].ToString());
    }

    private void CheckAvailableLevels() {
        int i = 1;
        foreach (var item in _levels.Keys) {
            if (_levels[item] > GameData.getInstance().Level) {
                item.interactable = false;
            }
            if (!PlayerPrefs.HasKey(Constants.LEVELS_KEY) && i == 1) {
                item.interactable = true;
            }
            i++;
        }
    }
}
