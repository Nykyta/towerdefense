using UnityEngine;

public class WayPointSytem : MonoBehaviour
{
    public int countPoint;
    public Vector3[] wayPointsVector3;

    [SerializeField] 
    private bool _debugModeCycle;
    [SerializeField] 
    private bool _debugMode;

    private void Start()
    {
        Init();
    }

    private void Init() 
    {
        countPoint = transform.childCount;
        wayPointsVector3 = new Vector3[countPoint];
        for (int i = 0; i < countPoint; i++) 
        {
            wayPointsVector3[i] = transform.GetChild(i).transform.position;
        }
    }

    private void DebugWayCycle(bool debug = false, bool debugCycle = false)
    {
        for (int i = 0; i < countPoint; i++)
        {
            if (debug && !debugCycle)
            {
                if (i != countPoint - 1)
                    Debug.DrawLine(wayPointsVector3[i], wayPointsVector3[i + 1], Color.red);
            }
            else if (!debug && debugCycle)
            {
                if (i != countPoint - 1)
                    Debug.DrawLine(wayPointsVector3[i], wayPointsVector3[i + 1], Color.red);
                else
                    Debug.DrawLine(wayPointsVector3[0], wayPointsVector3[countPoint - 1], Color.red);
            }
        }
    }
        
    private void OnDrawGizmos()
    {
        DebugWayCycle(_debugMode, _debugModeCycle);
    }
}
