using UnityEngine;

[CreateAssetMenu]

public class BonusData: ScriptableObject {
    public Sprite sprite;
    public Constants.Bonuses type;
}
