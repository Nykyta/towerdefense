using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]

public class StageForSpawnData : ScriptableObject
{
    public Constants.Enemies[] units;
}
