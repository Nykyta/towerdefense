using UnityEngine;

[CreateAssetMenu]

public class UnitData : ScriptableObject
{
    public int health;
    public int damage;
    public int cost;
    public float speed;
    public float timeShoot;
    public Sprite sprite;
    public float distanceOfView;
    public Constants.BulletType BulletType;
}
