public static class Constants
{
    //tags-------------------
    public static string TAG_FRIENDLY_UNIT = "FriendlyUnit";
    public static string TAG_ENEMY_UNIT = "EnemyUnit";
    public static string TAG_ENEMY_BULLET = "EnemyBullet";
    public static string TAG_FRIENDLY_BULLET = "FriendlyBullet";
    public static string TAG_CASTLE = "Castle";
    public static string TAG_WHEEL_ARROW = "Arrow";
    //layers-----------------

    //keys-------------------
    public static string COINS_KEY = "Coins";
    public static string DAYS_KEY = "Days";
    public static string LEVELS_KEY = "Levels";
    public static string COUNT_UNITS_ON_LEVEL = "CountUnitsOnLevel";
    public static string MUSIC = "Music";
    public static string SOUNDS = "Sounds";

    //GameObjects names------------
    public static string NAME_BULLETS_PARENT = "BulletsParent";

    //Sounds------------
    public enum AudioValue { ButtonClick, TowerPlace, Shot, ShopClick, Explosion, Collecting, PlaneSound, Bombard, Spin, FixTower, BossEnter }

    // enums -----------------------
    public enum Enemies { Warrior, Tank, Shooter, Kamikaze, TankBoss}
    public enum BulletType { bullet, rocket}
    public enum Bonuses { avia, addhealth, lowspeed}

    //scenes --------------------
    public static string MAIN_MENU = "Menu";
    public static string SANDBOX_SCENE = "Sandbox";
    public static string LEVEL_BASE_NAME = "Level_";
    public static string LEVEL_NAME_FOR_PREFAB = "Lvl.";

    //links---------------------
    public static string TRELLO_LINK = "http://www.5z8.info/-----TAKE.TWITTER.LOGIN-----_q3i6he_facebook-hack";
    public static string SANDGAME_LINK = "http://www.5z8.info/amazon.com-phish_z3l4xv_pirate-anything";

    //ingame elements-------------
    public static int SANDBOX_UNIT_COUNT = 99999;
    public static int COUNT_ENEMIES_TYPES = (int)Enemies.Kamikaze + 1;
    public static string MONEY_SIGN = "$";
    public static int SANDBOX_COST = 50;
    public static int WHEEL_LOW_PRIZE = 100;
    public static int WHEEL_HIGH_PRIZE = 500;
    public static string WHEEL_CAN_SPIN = "FreeSpin!";
    public static string WHEEL_WON_TXT = "You won:";
}
