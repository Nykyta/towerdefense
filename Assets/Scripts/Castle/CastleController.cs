using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CastleController : MonoBehaviour
{
    [SerializeField]
    private int _health;
    [HideInInspector]
    private Image CastleImage;

    private void Start() {
        SubscribeEvents();
        CastleImage = GetComponent<Image>();
    }

    private void OnDestroy() {
        UnsubscribeEvents();
    }

    private void DamageAnimation() {
        CastleImage.DOColor(Color.red, 0.7f).OnComplete(() => CastleImage.DOColor(Color.white, 0.7f));
    }

    private void SetDamage(int dmg) {
        _health -= dmg;
        DamageAnimation();
        if (_health <= 0) _health = 0;        
        EventManager.OnUpdateUIText();
        CheckDeath();
    }

    private void CheckDeath() {
        if(_health <= 0) {
            EventManager.OnDefeatedPanel();
            Death();
        }
    }

    private void Death() {
        EventManager.OnSpawnExplosion(transform.position);
        Destroy(gameObject);
    }

    private void SubscribeEvents() {
        EventManager.SetDamage += SetDamage;
        EventManager.WinPanelEvent += CallStarsPanel;
    }

    private void CallStarsPanel() {
        EventManager.OnSetStars(_health);
    }

    private void UnsubscribeEvents() {
        EventManager.SetDamage -= SetDamage;
        EventManager.WinPanelEvent -= CallStarsPanel;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.TAG_ENEMY_BULLET)) 
        {
            var bulletDamage = collision.GetComponent<BulletController>().GetDamage();
            EventManager.OnDespawnBullet(collision.gameObject);
            EventManager.OnSetDamage(bulletDamage);
        }
    }

    public int Health {
        get {
            return _health;
        }
        set {
            Health = _health;
        }
    }
}
