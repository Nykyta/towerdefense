using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FortuneWheel : MonoBehaviour
{
    [SerializeField]
    private Button _spinBtn;
    [SerializeField]
    private TMP_Text _superMoneyTxt;
    [SerializeField]
    private TMP_Text _moneyTxt;
    [SerializeField]
    private TMP_Text _profileMoney;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private TMP_Text _prizeValue;
    [SerializeField]
    private TMP_Text _wonText;
    private int _countOfSpins;
    private bool _isSpinning;
    private GameObject _prizeColider;
    private int _randFrom = -3240;
    private int _randTo = -1080;
    private float _wonTextSpeed = 1f;

    private void Start()
    {
        _wonText.text = Constants.WHEEL_WON_TXT;
        _spinBtn.onClick.AddListener(Spin);
    }

    private void OnEnable() {
        TurnWinText();
    }

    private void OnDisable() {
        _prizeValue.transform.localScale = new Vector3(1, 1, 1);
        _prizeValue.transform.DOKill();
    }

    private void Spin() {
        EventManager.OnPlaySound(Constants.AudioValue.Spin);
        if (!_isSpinning) {
            int prizeSum;
            _isSpinning = true;
            _countOfSpins = UnityEngine.Random.Range(_randFrom, _randTo);
            transform.DORotate(new Vector3(0, 0, _countOfSpins), _speed).OnComplete(() => {
                if (_prizeColider != null)
                    prizeSum = Constants.WHEEL_HIGH_PRIZE;
                else
                    prizeSum = Constants.WHEEL_LOW_PRIZE;
                GameData.getInstance().Coins += prizeSum;
                if (!_wonText.gameObject.active)
                    TurnWinText(true);

                _prizeValue.text = prizeSum.ToString();
                _profileMoney.text = GameData.getInstance().Coins.ToString();
                PlayerPrefs.SetInt(Constants.COINS_KEY, GameData.getInstance().Coins);
                _isSpinning = false;
            });
        }
    }

    private void TurnWinText(bool mode = false) {
        if (mode) {
            _prizeValue.gameObject.SetActive(true);
            _wonText.gameObject.SetActive(true);
            _prizeValue.gameObject.transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), _wonTextSpeed).SetLoops(-1, LoopType.Yoyo);
        }
        else {
            _prizeValue.gameObject.SetActive(false);
            _wonText.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag(Constants.TAG_WHEEL_ARROW))
            _prizeColider = collision.gameObject;
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.gameObject.CompareTag(Constants.TAG_WHEEL_ARROW))
            _prizeColider = null;
    }
}
