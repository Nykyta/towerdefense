using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StarsController : MonoBehaviour
{
    [SerializeField]
    private Image[] Stars;



    private void OnDisable() {
        EventManager.SetMark -= SetMarkEvent;
    }

    private void OnEnable() {
        EventManager.SetMark += SetMarkEvent;
    }

    private void SetMarkEvent(int castleHealth) {
        if (castleHealth == 500) {
            ActiveStars(2);
        } else if (castleHealth < 250) {
            ActiveStars(0);
        } else { ActiveStars(1);
        }
    }



    private void ActiveStars(int col) {
        for (int i = 2; i>col; i--) {
            Stars[i].color = Color.black;
        }
    }

  
}
