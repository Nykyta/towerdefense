using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DamageTextPoolObject : PoolObject<TMP_Text, DamageTextObjectData>
{
    protected override void ObjectBuilder(GameObject obj, DamageTextObjectData data) {
        obj.transform.position = data.pos;
        Objects[obj].text = data.value.ToString();
    }

    protected override void OnAddListeners() {
        EventManager.DamageTextEvent += SpawnObj;
        EventManager.DespawnDamageText += DespawnObj;
    }

    protected override void OnRemoveListeners() {
        EventManager.DamageTextEvent -= SpawnObj;
        EventManager.DespawnDamageText -= DespawnObj;
    }
}
