using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPoolObject :PoolObject<Bonus, BonusObjectData> {

    [SerializeField]
    protected List<BonusData> ObjData;

    protected override void ObjectBuilder(GameObject obj, BonusObjectData data) {
        int rnd = Random.Range(0, ObjData.Count);
        BonusData bonus = ObjData[rnd];
        Objects[obj].Init(bonus);
        obj.transform.position = data.pos;
    }

    protected override void OnAddListeners() {
        EventManager.SpawnNewBonus += SpawnObj;
        EventManager.DespawnBonus += DespawnObj;
    }

    protected override void OnRemoveListeners() {
        EventManager.SpawnNewBonus -= SpawnObj;
        EventManager.DespawnBonus -= DespawnObj;
    }
}
