using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPoolObject: PoolObject<BulletController, BulletObjectData> {
    protected override void ObjectBuilder(GameObject obj, BulletObjectData data) {
        Objects[obj].Init(data.towerController.Data.damage, data.towerController.Data.BulletType);
        obj.transform.position = data.pos;
        obj.transform.rotation = data.rot;
        obj.tag = data.tag;
    }
    
    protected override void OnAddListeners() {
        EventManager.SpawnNewBullet += SpawnObj;
        EventManager.DespawnBullet += DespawnObj;
    }

    protected override void OnRemoveListeners() {
        EventManager.SpawnNewBullet -= SpawnObj;
        EventManager.DespawnBullet -= DespawnObj;
    }
}
