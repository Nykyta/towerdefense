using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPoolObject : PoolObject<Explosion, ExplosionObjectData>
{
    protected override void ObjectBuilder(GameObject obj, ExplosionObjectData data) {
        obj.transform.position = data.pos;
    }

    protected override void OnAddListeners() {
        EventManager.SpawnNewExplosion += SpawnObj;
        EventManager.DespawnExplosion += DespawnObj;
    }

    protected override void OnRemoveListeners() {
        EventManager.SpawnNewExplosion -= SpawnObj;
        EventManager.DespawnExplosion -= DespawnObj;
    }

}
