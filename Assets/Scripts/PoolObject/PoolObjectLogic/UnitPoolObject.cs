using System.Collections.Generic;
using UnityEngine;

public class UnitPoolObject: PoolObject<DefaultUnit, UnitObjectData> {
    [SerializeField]
    private MonoBehaviour[] _components;
    [SerializeField]
    private bool _isCycleUnit;
    [SerializeField]
    protected List<UnitData> ObjData;

    protected override void ObjectBuilder(GameObject obj, UnitObjectData data) {

        var component = obj.AddComponent(_components[(int)data.id].GetType()); 
        UnitData objData = ObjData[(int)data.id];
        DefaultUnit unit = component.GetComponent<DefaultUnit>();
        unit.unitData = objData;
        unit.InitParameters(objData);
        unit.tag = data.tag;
        unit.wayPointSystemUnit = data.waypoints;
        unit.hpBar = component.transform.GetChild(0).gameObject; // HealthBar index 0
        unit.transform.position = data.pos;
        unit.cycleMovementUnit = _isCycleUnit;     
    }

    protected override void OnAddListeners() {
        EventManager.SpawnNewUnit += SpawnObj;
        EventManager.DespawnUnit += DespawnObj;
    }

    protected override void OnRemoveListeners() {
        EventManager.SpawnNewUnit -= SpawnObj;
        EventManager.DespawnUnit -= DespawnObj;
    }
}
