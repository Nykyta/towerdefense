using System.Collections.Generic;
using UnityEngine;

public abstract class PoolObject<T,K> : MonoBehaviour {
    [SerializeField]
    private int poolCount;
    [SerializeField]
    private GameObject objPrefab;

    public static Dictionary<GameObject, T> Objects;
    protected Queue<GameObject> currentObjects;
    protected abstract void ObjectBuilder(GameObject obj, K objectData);

    private void Awake() {
        Init();
    }

    private void Init() {
        Objects = new Dictionary<GameObject, T>();
        currentObjects = new Queue<GameObject>();
        InitPool();
    }

    private void InitPool() {
        for (int i = 0; i < poolCount; i++) {
            GameObject prefab = Instantiate(objPrefab, transform);
            T script = prefab.GetComponent<T>();
            prefab.SetActive(false);
            Objects.Add(prefab, script);
            currentObjects.Enqueue(prefab);
        }
    }

    public void SpawnObj(K objectData) {
        GameObject obj = currentObjects.Dequeue();
        ObjectBuilder(obj, objectData);
        obj.SetActive(true);
    }

    protected void DespawnObj(GameObject obj) {
        obj.transform.position = transform.position;
        obj.SetActive(false);
        currentObjects.Enqueue(obj);
    }
    private void OnEnable() {
        OnAddListeners();
    }
    private void OnDisable() {
        OnRemoveListeners();
    }

    protected abstract void OnAddListeners();

    protected abstract void OnRemoveListeners();

}
