using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObjectData {
    
    public readonly Vector3 pos;
    public BonusObjectData(Vector3 Pos) {
        pos = Pos;
    }
}
