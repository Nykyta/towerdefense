using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletObjectData
{
    public readonly TowerController towerController;
    public readonly Vector3 pos;
    public readonly Quaternion rot;
    public readonly string tag;
    public BulletObjectData(TowerController TowerController, Vector3 Pos, Quaternion Rot, string Tag) {
        towerController = TowerController;
        pos = Pos;
        rot = Rot;
        tag = Tag;
    }
}
