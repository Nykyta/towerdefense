using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionObjectData
{
    public readonly Vector3 pos;
    public ExplosionObjectData(Vector3 Pos) {
        pos = Pos;
    }
}
