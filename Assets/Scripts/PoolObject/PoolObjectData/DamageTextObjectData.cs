using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextObjectData
{
    public readonly int value;
    public readonly Vector3 pos;
    public DamageTextObjectData(int Value, Vector3 Pos) {
        value = Value;
        pos = Pos;
    }
}
