using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitObjectData {
    public readonly Constants.Enemies id;
    public readonly Vector3 pos;
    public readonly GameObject waypoints;
    public readonly string tag;
    public UnitObjectData(Constants.Enemies Id, Vector3 Pos, GameObject Waypoints, string Tag) {
        id = Id;
        pos = Pos;
        waypoints = Waypoints;
        tag = Tag;
    }
}
