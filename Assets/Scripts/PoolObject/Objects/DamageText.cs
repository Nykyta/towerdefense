using UnityEngine;
using DG.Tweening;

public class DamageText : MonoBehaviour
{
    private Vector3 BigScale = new Vector3(1.5f, 1.5f, 1.5f);
    private Vector3 NormScale = new Vector3(1f, 1f, 1f);
    private const float TIME_TO_BIGSCALE = 0.5f;
    private const float TIME_TO_NORMSCALE = 0.2f;

    private void OnEnable() {
        transform.DOScale(BigScale, TIME_TO_BIGSCALE).OnComplete(() => { transform.DOScale(NormScale, TIME_TO_NORMSCALE); EventManager.OnDespawnDamageText(gameObject); });
    }

}
