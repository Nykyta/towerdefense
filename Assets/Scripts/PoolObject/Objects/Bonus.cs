using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Bonus : MonoBehaviour
{
    [SerializeField]
    private Image img;
    private Constants.Bonuses type;
    private Vector3 smallSize = new Vector3(0f, 0f, 0f);
    private Vector3 normsize = new Vector3(1f, 1f, 1f);

    private void OnEnable() {
        transform.DOShakeRotation(2f, 50f, 5, 30f).OnComplete(() => transform.DOScale(smallSize, 2f).OnComplete(() => {
            EventManager.OnStartBonus(type);
            EventManager.OnPlaySound(Constants.AudioValue.Collecting);
            EventManager.OnDespawnBonus(gameObject);
        }));
    }

    private void OnDisable() {
        transform.DOKill();
        transform.localScale = normsize;
    }

    public void Init(BonusData bdata) {
        img.sprite = bdata.sprite;
        type = bdata.type;
    }
}
