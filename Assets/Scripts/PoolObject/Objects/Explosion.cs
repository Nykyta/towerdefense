using System.Collections;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private const float TIME_OF_EXPLOSION = 0.6f;
    private void OnEnable()
    {
        StartCoroutine(DestroyAniamtion());
    }

    private IEnumerator DestroyAniamtion() {
        var time = new WaitForSeconds(TIME_OF_EXPLOSION);
        yield return time;
        EventManager.OnDespawnExplosion(gameObject);
    }
}
