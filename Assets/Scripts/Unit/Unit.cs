using UnityEngine;
using UnityEngine.UI;

public abstract class Unit : MonoBehaviour,
    IUnitMovement,
    IUnitAttack,
    IUnitAnimation,
    IUnitParameters
{
    #region ANIMATION_UNIT
    public virtual void InitAnimator() 
    {
       animator = transform.GetComponent<Animator>();
    }
    public virtual void PlayIDLEAnimation() 
    {
    
    }
    public virtual void PlayWalkAnimation() 
    {
    
    }
    public virtual void PlayAttackAnimation() 
    {
    
    }

    public virtual Animator animator { get; set; }
    #endregion

    #region ATTACK_UNIT
    public virtual int Attack() 
    {
        IsMove = false;
        IsAttack = true;
        return Damage * 1; // Multiplicator
    }

    public virtual void CheckAttackTarget(Collider2D collision, string tagUnit, string tagBuilding) 
    {
        if (tag.Equals(tagUnit))
        {
            if (collision.tag.Equals(tagBuilding))
            {
                if (TimerAttack > 0) TimerAttack -= Time.fixedDeltaTime;
                else
                {
                    LookOnTarget(collision);
                    EventManager.OnSetDamage(Attack());
                    TimerAttack = TimeShoot;
                }
            }
        }
    }

    public virtual void LookOnTarget(Collider2D collision) 
    {
        Vector3 eulerAngles = transform.eulerAngles;
        Vector3 direction = (collision.transform.position - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        eulerAngles.z = angle;
        transform.eulerAngles = eulerAngles;
    }

    public float TimerAttack { get; set; }
    public bool IsAttack { get; set; }
    #endregion
    private const int UP_RANGE_OF_BONUS = 25;
    private const int SUCCESS_ID_OF_BONUS = 15;
    #region PARAMETERS_UNIT 
    public virtual void InitParameters(UnitData data) 
    {
        UnitData = data;
        Health = UnitData.health;
        Damage = UnitData.damage;
        Cost = UnitData.cost;
        Speed = UnitData.speed;
        TimeShoot = UnitData.timeShoot;
        transform.GetComponent<Image>().sprite = UnitData.sprite;
    }

    public virtual void CheckHealth() 
    {
        if (Health <= 0) 
        {
            Health = 0;
            EventManager.OnSpawnExplosion(transform.position);
            EventManager.OnPlaySound(Constants.AudioValue.Explosion);
            int coins = GetCoins();
            EventManager.OnDamageText(coins, transform.position);
            if (IsBonusPossible()) {
                EventManager.OnSpawnBonus(transform.position);
            }
            EventManager.OnDespawnUnit(gameObject);
            GameData.getInstance().Coins += coins;
            EventManager.OnUpdateUIText();
        }
    }

    private bool IsBonusPossible() {
        if (Random.Range(1, UP_RANGE_OF_BONUS) == SUCCESS_ID_OF_BONUS) {
            
            return true;
        }
        else return false;
    }
    
    public virtual int GetCoins() 
    {
        return (int)(Cost + Damage + Speed)/3;
    }

    public virtual int unityQuality() 
    {
        return (int)Speed + Damage + Cost;
    }
    public virtual void CheckCollision(Collider2D collision, string tagUnit, string tagBullet) 
    {
        if (tag.Equals(tagUnit))
            if (collision.tag.Equals(tagBullet))
            {
                var bullet = collision.GetComponent<BulletController>();

                //collision despawn bullet
                //EventManager.OnDamageText(-bullet.GetDamage(), transform.position);
                Health -= bullet.GetDamage(); // Parameters of bullet collision.damage
                CheckHealth();
                // Despawn bullet
                EventManager.OnDespawnBullet(collision.gameObject);
            }
    }
    public UnitData UnitData { get; set; }
    public int Health { get; set; }
    public int Damage { get; set; }
    public int Cost { get; set; }
    public float Speed { get; set; }
    public float speedOffset = 1f;
    public float TimeShoot { get; set; }
    public Sprite Sprite { get; set; }
    #endregion

    #region MOVEMENT_UNIT
    public virtual void InitMovement(GameObject obj) 
    {
        WayPointSystem = obj;
        UnitWay = WayPointSystem.GetComponent<WayPointSytem>().wayPointsVector3;
        CountPoint = WayPointSystem.GetComponent<WayPointSytem>().countPoint;
    }
    public virtual void MovementUnit(bool cycle = true) 
    {
        
        Vector3 unitPosition = transform.position;
        
        unitPosition = Vector3.MoveTowards(unitPosition, UnitWay[UnitOnPoint], Time.deltaTime * Speed * speedOffset * 0.1f);
        //Debug.Log(Time.deltaTime * speed * speedOffset * 0.1f);
        IsMove = true;
        if (UnitOnPoint == CountPoint - 1 && !cycle) UnitOnPoint = CountPoint - 1;
        else
            if (unitPosition == UnitWay[UnitOnPoint]) UnitOnPoint++;
        if (UnitOnPoint == CountPoint && cycle) UnitOnPoint = 0;
        IsAttack = false;
        transform.position = unitPosition;
    }

    public virtual void RotateUnitToPoint() 
    {
        Vector3 eulerAngles = transform.eulerAngles;
        Vector3 direction = (UnitWay[UnitOnPoint] - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        eulerAngles.z = angle;
        transform.eulerAngles = eulerAngles;
    }

    public int UnitOnPoint { get; set; }
    public GameObject WayPointSystem { get; set; }
    public bool CycleMovement { get; set; }
    public Vector3[] UnitWay { get; set; }
    public int CountPoint { get; set; }
    public bool IsMove { get; set; }
    #endregion

}
