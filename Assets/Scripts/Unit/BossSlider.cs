using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BossSlider : MonoBehaviour
{
    private int startUnitCount;
    private int currentUnitCount;
    [SerializeField]
    private Slider sliderboss;
    private GameData gameData = GameData.getInstance();
    // Start is called before the first frame update

    private void Awake() {
       currentUnitCount = 0;
    }

    private void ChangeSlider() {
        currentUnitCount++;
        float value = (float)(currentUnitCount) / (float)startUnitCount;
        if (value == Mathf.Infinity) {
            value = 0;
        }
        MoveProgressBar(value);
    }
    private void StateCount() {
            startUnitCount = gameData.CountUnitsOnLevel;
    }

    private void MoveProgressBar(float value) {
        sliderboss.DOValue(value, 1f);
        CheckForBossSound();
    }

    private void CheckForBossSound() { 
    if (startUnitCount==currentUnitCount) {
            EventManager.OnPlaySound(Constants.AudioValue.BossEnter);
        }
    }

    private void OnDisable() {
        EventManager.ChangeSliderBarEvent -= ChangeSlider;
        EventManager.StateCountOfUnitsEvent -= StateCount;
    }

    private void OnEnable() {
        EventManager.ChangeSliderBarEvent += ChangeSlider;
        EventManager.StateCountOfUnitsEvent += StateCount;
    }

}
