using UnityEngine;

interface IUnitAttack
{
    int Attack();
    public void CheckCollision(Collider2D collision, string tagUnit, string tagBullet);
    public void LookOnTarget(Collider2D collision);

    float TimerAttack { get; set; }
    bool IsAttack { get; set; }
}
