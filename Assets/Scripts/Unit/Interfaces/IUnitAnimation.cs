using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IUnitAnimation
{
    Animator animator 
    { get; set; }

    void InitAnimator();
    void PlayIDLEAnimation();
    void PlayWalkAnimation();
    void PlayAttackAnimation();
}
