using UnityEngine;

interface IUnitParameters
{
    void InitParameters(UnitData data);
    int unityQuality();
    void CheckHealth();
    void CheckCollision(Collider2D collision, string tagUnit, string tagBullet);
    int GetCoins();

    UnitData UnitData { get; set; }
    int Health { get; set; }
    int Damage { get; set; }
    int Cost { get; set; }
    float Speed { get; set; }
    float TimeShoot { get; set; }
    Sprite Sprite { get; set; }
}
