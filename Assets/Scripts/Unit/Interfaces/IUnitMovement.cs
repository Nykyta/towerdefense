using UnityEngine;

interface IUnitMovement
{
    void InitMovement(GameObject obj);
    void MovementUnit(bool cycle = true);
    void RotateUnitToPoint();

    int UnitOnPoint { get; set; }
    GameObject WayPointSystem { get; set; }
    float Speed { get; set; }
    bool CycleMovement { get; set; }
    Vector3[] UnitWay { get; set; }
    int CountPoint { get; set; }
    bool IsMove { get; set; }
}
