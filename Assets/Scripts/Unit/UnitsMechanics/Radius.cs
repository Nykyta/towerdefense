using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radius : MonoBehaviour
{
    private GameObject _target = null;

    public GameObject Target
    {   get 
        {
            return _target;
        } 
        set 
        {
            Target = _target;
        } 
    }
    
    private void Start()
    {
        Physics2D.IgnoreLayerCollision(8, 6);
        Physics2D.IgnoreLayerCollision(8, 7);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.TAG_FRIENDLY_UNIT))
            _target = collision.gameObject;
    }
}
