using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KamikazeUnit : DefaultUnit
{
    public override void FixedUpdate()
    {
        if (unitData != null && !IsAttack) MovementUnit(cycleMovementUnit);
        if (IsMove && !IsAttack) RotateUnitToPoint();
        if (IsAttack) CheckHealthAttack();
        CheckValueOnHpBar();
    }
    private void CheckHealthAttack() 
    {
        if (Health <= 0)
        {
            EventManager.OnSpawnExplosion(transform.position);
            EventManager.OnDespawnUnit(gameObject);
        }
    }
    public override int Attack() 
    {
        IsMove = false;
        IsAttack = true;
        Health = -1;
        return Damage * 1;
    }
}
