using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DefaultUnit : Unit
{
    
    public GameObject hpBar;
    public bool cycleMovementUnit;
    [HideInInspector]
    public Slider _hpBarSlider;
    public UnitData unitData;
    public GameObject wayPointSystemUnit;

    private void OnEnable()
    {
        EventManager.ChangeSpeedEvent += ChangeUnitSpeed;
        EventManager.AviaBombardEvent += BombardReaction;
    }

    private void OnDisable() {
        EventManager.ChangeSpeedEvent -= ChangeUnitSpeed;
        EventManager.AviaBombardEvent -= BombardReaction;
        DestroyScript();
        CheckCountUnitsOnMap();
    }

    public void BombardReaction() {
        
         Health = Health / 2;        
    }

    public virtual void Start()
    {
        InitUnit();
    }

    public virtual void FixedUpdate()
    {
        if (unitData != null && !IsAttack) MovementUnit(cycleMovementUnit);
        if(IsMove && !IsAttack)RotateUnitToPoint();
        CheckValueOnHpBar();
    }

    public virtual void InitUnit()
    {
        InitMovement(wayPointSystemUnit);
        InitHpBar();
    }

    public void InitHpBar() 
    {
        _hpBarSlider = hpBar.GetComponent<Slider>();
        _hpBarSlider.maxValue = Health;
    }

    public void CheckValueOnHpBar() 
    {
        _hpBarSlider.DOValue(Health, 1f);
    }

    public void DestroyScript() 
    {
        Destroy(this);
    }
    public virtual void CheckCountUnitsOnMap()
    {   GameData.getInstance().CountUnitsOnLevel -= 1;
        if (GameData.getInstance().CountUnitsOnLevel <= 0)
            EventManager.OnWinPanel();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        CheckCollision(collision, Constants.TAG_ENEMY_UNIT, Constants.TAG_FRIENDLY_BULLET);
        CheckCollision(collision, Constants.TAG_FRIENDLY_UNIT, Constants.TAG_ENEMY_BULLET);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        CheckAttackTarget(collision, Constants.TAG_ENEMY_UNIT, Constants.TAG_CASTLE);
    }

    protected void ChangeUnitSpeed() {
        speedOffset = 0.5f;
        StartCoroutine(NormalizedSpeed());
    }

    protected IEnumerator NormalizedSpeed() {
        var time = new WaitForSeconds(5f);
        speedOffset = 0.5f;
        yield return time;
        speedOffset = 1f;
    }
}
