using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterUnit : DefaultUnit
{
    private GameObject _radiusObj;
    private Radius _radius;
    private TowerController _towerController;

    private void OnDisable() {
        EventManager.ChangeSpeedEvent -= ChangeUnitSpeed;
        EventManager.AviaBombardEvent -= BombardReaction;
        DestroyScript();
        CheckCountUnitsOnMap();
        _radiusObj.SetActive(!_radiusObj.activeInHierarchy);
    }

    public override void Start()
    {
        InitUnit();
    }

    public override void FixedUpdate()
    {
        if (unitData != null && !IsAttack) MovementUnit(cycleMovementUnit);
        if (IsMove && !IsAttack) RotateUnitToPoint();
        if (_radius.Target == null)
            ObstacleUnAvailable();
        CheckValueOnHpBar();
    }

    public override void InitUnit()
    {
        _radiusObj = transform.GetChild(1).gameObject;
        _radiusObj.SetActive(!_radiusObj.activeInHierarchy);
        _radius = _radiusObj.GetComponent<Radius>();
        _towerController = _radiusObj.GetComponent<TowerController>();

        InitMovement(wayPointSystemUnit);
        InitHpBar();
    }
    private IEnumerator Shot()
    {
        EventManager.OnSpawnBullet(_towerController, transform.position, _radiusObj.transform.rotation, Constants.TAG_ENEMY_BULLET) ;
        EventManager.OnPlaySound(Constants.AudioValue.Shot);
        yield return new WaitForSeconds(_towerController.Data.timeShoot);
        if (_radius.Target)
            StartCoroutine(Shot());
    }

    public virtual Vector3 Rotate()
    {
        Vector3 eulerAngles = transform.eulerAngles;
        Vector3 direction = (UnitWay[UnitOnPoint] - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        eulerAngles.z = angle;
        return eulerAngles;
    }
    private void ObstacleAvailable(Collider2D collision) 
    {
        LookOnTarget(collision);
        IsMove = false;
        IsAttack = true;
        StartCoroutine(Shot());
    }
    private void ObstacleUnAvailable()
    {
        IsMove = true;
        IsAttack = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_radius.Target != null)
            if (collision.name.Equals(_radius.Target.name))
            {
                ObstacleAvailable(collision);
            }
        CheckAttackTarget(collision, Constants.TAG_ENEMY_UNIT, Constants.TAG_CASTLE);
        CheckCollision(collision, Constants.TAG_ENEMY_UNIT, Constants.TAG_FRIENDLY_BULLET);
        CheckCollision(collision, Constants.TAG_FRIENDLY_UNIT, Constants.TAG_ENEMY_BULLET);
    }
    public override void CheckAttackTarget(Collider2D collision, string tagUnit, string tagBuilding)
    {
        if (tag.Equals(tagUnit))
        {
            if (collision.tag.Equals(tagBuilding))
            {
                IsMove = false;
                IsAttack = true;
                if (TimerAttack > 0) TimerAttack -= Time.fixedDeltaTime;
                else
                {
                    ObstacleAvailable(collision);
                    TimerAttack = TimeShoot;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (_radius.Target == null)
            ObstacleUnAvailable();
    }

}
