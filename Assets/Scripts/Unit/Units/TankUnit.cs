using UnityEngine;

public class TankUnit : DefaultUnit
{
    private int _maxHealth;
    private float _timerRecoveryHealth;
    private float _cooldownRecoveryHealth = 5f;

    public override void Start()
    {
        InitUnit();
    }

    public override void InitUnit() 
    {
        _maxHealth = Health;
        _timerRecoveryHealth = _cooldownRecoveryHealth;
        InitMovement(wayPointSystemUnit);
        InitHpBar();
    }

    public override void FixedUpdate()
    {
        if (unitData != null && !IsAttack) MovementUnit(cycleMovementUnit);
        if (IsMove && !IsAttack) RotateUnitToPoint();
        CheckValueOnHpBar();
        RecoveryHealth();
    }

    private void RecoveryHealth() 
    {
        if (_timerRecoveryHealth > 0) _timerRecoveryHealth -= Time.fixedDeltaTime;
        else
        {
            if (Health >= _maxHealth) Health = _maxHealth;
            else Health += _maxHealth/20; 
       
            _timerRecoveryHealth = _cooldownRecoveryHealth;
        }
    }
}
